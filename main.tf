provider "aws" {
  region = "us-east-1"
}

variable "vpc_cidr_block" {}
variable "env" {}
variable "subnet_cidr_block" {}
variable "avail_zone" {}
variable "my_ip" {}

resource "aws_vpc" "my_app_vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    Name : "${var.env}-vpc"
  }
}

resource "aws_subnet" "my_app_subnet" {
  vpc_id            = aws_vpc.my_app_vpc.id
  cidr_block        = var.subnet_cidr_block
  availability_zone = var.avail_zone

  tags = {
    Name : "${var.env}-subnet"
  }
}