

resource "aws_route_table" "my-app-rtb" {
  vpc_id = aws_vpc.my_app_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "${var.env}-rtb"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.my_app_vpc.id
  tags = {
    Name = "${var.env}-igw"
  }

}


resource "aws_route_table_association" "rtb-association" {
  subnet_id      = aws_subnet.my_app_subnet.id
  route_table_id = aws_route_table.my-app-rtb.id
}