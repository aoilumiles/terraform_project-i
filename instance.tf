variable "instance_type" {}
variable "secret_key" {}

data "aws_ami" "my_recent_ami" {

  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-*-x86_64-gp2"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

output "my_recent_ami" {
  value = data.aws_ami.my_recent_ami.id
}

output "public_ip" {
  value = aws_instance.my_app_instance.public_ip
}
/*resource "aws_key_pair" "key_secret_name" {
  key_name   = "secret-key"
  public_key = file(var.secret_key)
}*/
resource "aws_instance" "my_app_instance" {
  ami                         = data.aws_ami.my_recent_ami.id
  instance_type               = var.instance_type
  subnet_id                   = aws_subnet.my_app_subnet.id
  associate_public_ip_address = true
  security_groups             = [aws_security_group.my-app-sg.id]
  availability_zone           = var.avail_zone
  /*key_name                    = aws_key_pair.key_secret_name.key_name*/

  user_data = file("entry_script.sh")
  tags = {
    Name = "${var.env} - instance"
  }
}